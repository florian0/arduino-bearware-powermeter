#ifndef STECKDOSE_ARDUINO_BITBUFFER_H
#define STECKDOSE_ARDUINO_BITBUFFER_H

#include <Arduino.h>
#include "TogglePin.h"

class BitBuffer {
public:
    void Setup();

    void PushBit(bool value);

    void Clear();

    void Print();

    float GetVoltage() const;

    uint16_t GetPower() const;

private:
    int bitPos = 0;
    int bytePos = 0;
    uint8_t buffer[10] = {0};


    TogglePin<7> bitFlip;
    TogglePin<5> byteFlip;
};


#endif //STECKDOSE_ARDUINO_BITBUFFER_H
