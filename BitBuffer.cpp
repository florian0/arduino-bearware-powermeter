#include "BitBuffer.h"

#define ARRAY_LENGTH(arr) (sizeof(arr)/sizeof(*arr))

void BitBuffer::PushBit(bool value) {
    if (bitPos >= 8) {
        bitPos = 0;
        bytePos++;
        byteFlip.Toggle();
#ifdef DEBUGBITS
        Serial.print(' ');
#endif
    }
    bitFlip.Toggle();
    buffer[bytePos] |= ((value & 1) << (bitPos++));
#ifdef DEBUGBITS
    Serial.print(value);
#endif
}

void BitBuffer::Setup() {
    byteFlip.Setup();
    bitFlip.Setup();
}

void BitBuffer::Clear() {
    for (int i = 0; i < ARRAY_LENGTH(buffer); i++) {
        buffer[i] = 0;
    }

    bitPos = 0;
    bytePos = 0;
}

void BitBuffer::Print() {
#ifdef DEBUGBYTES
    Serial.print("Buffer: (size:");
    Serial.print(bytePos);
    Serial.print(')');
    for (int i = 0; i < bytePos; i++) {
        Serial.print(buffer[i]);
        Serial.print(' ');
    }
    Serial.print(' ');
#endif
    Serial.print(GetVoltage());
    Serial.print(" V | ");
    Serial.print(GetPower());
    Serial.print(" mA");
    Serial.println();
}

float BitBuffer::GetVoltage() const {
    uint8_t offset = buffer[2];
    return (2304 + offset) / 10.0f;
}

uint16_t BitBuffer::GetPower() const {
    return buffer[4] | (buffer[5] << 7);
}
