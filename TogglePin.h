#ifndef STECKDOSE_ARDUINO_TOGGLEPIN_H
#define STECKDOSE_ARDUINO_TOGGLEPIN_H

#include <Arduino.h>

template<int Pin>
class TogglePin {
public:

    void Setup() {
        pinMode(Pin, OUTPUT);
        digitalWrite(Pin, state);
    }

    void Toggle() {
        digitalWrite(Pin, state = !state);
    }

private:
    bool state = false;
};


#endif //STECKDOSE_ARDUINO_TOGGLEPIN_H
