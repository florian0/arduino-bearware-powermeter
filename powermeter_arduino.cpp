#include <Arduino.h>
#include "BitBuffer.h"

const int dataInput = A0;
const int deadPin = 6;
const int activePin = 3;
const int outputPin = 2;

BitBuffer buffer;

const unsigned long longThreshold = 170;
const unsigned long endThreshold = 600;
const unsigned long startThreshold = 1800;

int oldPinValue;
unsigned long previousEdgeTime;

#define IMDEAD() do { digitalWrite(deadPin, 1); Serial.println("Me Dead"); while(1) {}; } while(0)

#define IsStart(length) (length > startThreshold)
#define IsLong(length) (length > longThreshold)
#define IsEnd(length) (length > endThreshold)

enum states {
    IDLE,
    PROCESS_START,
    RECEIVE,
    LONG,
    SHORT
};

states currentState;

void setup() {

    Serial.begin(115200);

    pinMode(dataInput, INPUT);
    pinMode(A1, INPUT);
    pinMode(A2, INPUT);
    pinMode(outputPin, OUTPUT);

    buffer.Setup();
    buffer.Clear();

    pinMode(deadPin, OUTPUT);

    pinMode(activePin, OUTPUT);

    digitalWrite(activePin, 0);

    previousEdgeTime = 0;
    currentState = IDLE;
}

void loop() {
    states nextState;
    int newPinValue = digitalRead(dataInput);

    digitalWrite(activePin, (currentState != IDLE));

    if (newPinValue == oldPinValue)
        return;

    // store time
    unsigned long edgeTime = micros();
    unsigned long edgeLength = edgeTime - previousEdgeTime;

    bool isRisingEdge = (newPinValue == 1);

    switch (currentState) {
        case IDLE:
            digitalWrite(outputPin, 0);
            if (isRisingEdge) {
                nextState = PROCESS_START;
            }
            break;

        case PROCESS_START:
            if (IsStart(edgeLength)) {
                nextState = RECEIVE;
                buffer.Clear();
            } else {
                nextState = IDLE;
            }
            break;

        case RECEIVE:
            if (IsEnd(edgeLength)) {
                //Serial.println("This is the end (0)");
                nextState = IDLE;
            } else if (IsLong(edgeLength) && !IsEnd(edgeLength)) {
                nextState = LONG;
            } else {
                nextState = SHORT;
            }
            break;

        case LONG:
            if (IsEnd(edgeLength)) {
                //Serial.println("This is the end (1)");
                buffer.Print();
                nextState = IDLE;
            } else if (!IsLong(edgeLength)) {
                buffer.PushBit(1);
                digitalWrite(outputPin, 1);
                nextState = RECEIVE;
            } else {
                IMDEAD();
            }
            break;
        case SHORT:
            if (IsEnd(edgeLength)) {
                //Serial.println("This is the end (2)");
                buffer.Print();
                nextState = IDLE;
            } else if (IsLong(edgeLength)) {
                buffer.PushBit(0);
                digitalWrite(outputPin, 0);
                nextState = RECEIVE;
            } else {
                IMDEAD();
            }
            break;
    }

    previousEdgeTime = edgeTime;
    currentState = nextState;
    oldPinValue = newPinValue;
}
